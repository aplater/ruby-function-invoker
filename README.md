# ruby-function-invoker

[![Coverage report](https://gitlab.com/gitlab-org/gitlab-ce/badges/master/coverage.svg?job=coverage)](http://gitlab-org.gitlab.io/gitlab-ce/coverage-ruby)
[![pipeline status](https://gitlab.com/Alexand/ruby-function-invoker/badges/master/pipeline.svg)](https://gitlab.com/Alexand/ruby-function-invoker/commits/master)

This repo is used to build a Docker image of a simple Rack Application that runs a puma HTTP server.

## How does it work

* Runs on port 8080
* It only listens for GET and POST requests. Other methods return 404 response.
* It does not route, thus any path will call the function.
* Success (200) responses are always `Content-Type:application/json'` and `JSON.dump(response)`. Where `response` is the output of your function.
* Autoloads whatever ruby files are found under `/functions/`.
* It expects `FUNCTION_HANDLER` environment variable to be defined with the method to be called.
* Functions will be called with a hash containing `event` and `context` keys as arguments, where:

  `event`: corresponds to the resquest parsed params. It accepts query string and/or payload params.

  `context`: corresponds to the rack request env.

### Examples of functions

1. Object scoped functions

```ruby
# my_function.rb

def hello(event:, context:)
  "Hello World"
end
```

In this case we need to set environment variable `FUNCTION_HANDLER=hello`

2. Class scoped functions

```ruby
# foo.rb

class Foo
  def bar(event:, context:)
    "Hello World"
  end
end
```

In this case we need to set environment variable `FUNCTION_HANDLER="Foo.bar"`

### How to test it locally

1. To create your function and put it in, for instance, a `~/functions` folder:

```ruby
# ~/functions/function.rb

def gandalf_is(event:, context:)
  "A wizard is never #{event["word"]} !"
end
```

2. Start you function invoker:

```bash
docker run  -p 8080:8080 \
            -v ~/functions:/functions \
            -it \
            --rm \
            --env FUNCTION_HANDLER="gandalf_is" \
            alexand/ruby-function-invoker:0.1.0-alpha
```

3. Call your function:

```bash
curl -v http://localhost:8080/?word=late
```