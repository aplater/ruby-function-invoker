FROM ruby:2.6-alpine

WORKDIR /usr/src/app

# throw errors if Gemfile has been modified since Gemfile.lock
RUN bundle config --global frozen 1

COPY Gemfile Gemfile.lock ./

# bundle is together with the packages installation so we can reduce the image
# size by 150Mib by deleting the build-base native extension. If the removal
# of this package is in a later docker layer, we can't reclaim image size.
RUN apk update && \
    apk upgrade && \
    apk add build-base && \
    bundle install --without test development && \
    apk del build-base && \
    rm -rf /var/cache/apk/*

COPY . .

EXPOSE 8080

CMD ["rackup", "-p", "8080", "-o", "0.0.0.0"]
