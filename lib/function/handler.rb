# frozen_string_literal: true

module Function
  class Handler
    ARG_ERR_MSG = 'Handler must be either a `method_name` or' \
                  'ClassName.method_name` for class methods'

    def initialize(handler)
      @handler_class, @handler_method = parse(handler)
    end

    def call(args)
      if @handler_class
        Kernel.const_get(@handler_class)
              .send(@handler_method, args)
      else
        __send__(@handler_method, args)
      end
    end

    private

    def parse(handler)
      handler_split = handler&.split('.')

      return [nil, handler_split.first] if handler_split&.size == 1
      return handler_split if handler_split&.size == 2

      raise ArgumentError, ARG_ERR_MSG
    end
  end
end
