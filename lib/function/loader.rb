# frozen_string_literal: true

module Function
  class Loader
    def self.load!
      Dir['/functions/**/*.rb'].each { |f| load(f) }

      yield if block_given?
    end
  end
end
