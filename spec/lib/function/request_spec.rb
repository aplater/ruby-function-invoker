# frozen_string_literal: true

describe Function::Request do
  subject do
    described_class.new(request)
  end

  let(:request) do
    Rack::Request.new(env)
  end

  context 'when sending form encoded params' do
    let(:env) do
      opts = { method: 'POST',
               input: 'abc=value&def=another',
               'CONTENT_TYPE' => 'application/x-www-form-urlencoded' }

      Rack::MockRequest.env_for('/?param=test', opts)
    end

    it 'returs a hash of decoded params' do
      expect(subject.params)
        .to eq('param' => 'test', 'abc' => 'value', 'def' => 'another')
    end
  end

  context 'when sending JSON payload' do
    let(:env) do
      Rack::MockRequest
        .env_for('/?param=test', method: 'POST',
                                 input: '{"abc":"payload"}',
                                 'CONTENT_TYPE' => 'application/json')
    end

    it 'returs a hash of decoded params' do
      expect(subject.params)
        .to eq('param' => 'test', 'abc' => 'payload')
    end
  end
end
